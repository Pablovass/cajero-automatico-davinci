package com.pablovallejos;

import java.util.Scanner;

import com.pablovallejos.dao.BilleteDao;
import com.pablovallejos.dao.BilleteDaoImpl;
import com.pablovallejos.dao.PersonaDao;
import com.pablovallejos.dao.PersonaDaoImpl;
import com.pablovallejos.db.Login;
import com.pablovallejos.db.PreLogin;
import com.pablovallejos.pojo.Billete;
import com.pablovallejos.pojo.Persona;

public class TestCajero {

	public static void main(String[] arg) {

		Scanner scanner = new Scanner(System.in);
		String nam;
		String password;
		int transaccion;
		int dineroGuardado;

		double total;

		System.out.println("BIENVENIDO AL BANCO AMERICA");
		System.out.println("Ingrese su usuario");
		nam = scanner.nextLine();
		System.out.println("Ingrese su contrasena");
		password = scanner.nextLine();

		PreLogin preLogin = new PreLogin();

		preLogin.setBusqueda(nam, password);

		if (preLogin.getBusqueda() == true) {
			// intancio mi login
			Login cliente = Login.getInstancia();

			boolean clienteLog = cliente instanceof Login;

			if (clienteLog == true) {

				// id DEL cliente -1 porque no existe el cliente cero
				int idCliente = (preLogin.getId() - 1);

				Persona client = new Persona();
				PersonaDao clienteDao = new PersonaDaoImpl();

				Billete billete = new Billete();
				BilleteDao billeteDao = new BilleteDaoImpl();

				System.out.println("las opciones disponibles son: ");
				System.out.println(" Ingreso de dinero -1\n Retiro de dinero  -2\n Consultar saldo   -3\n");
				int opcion = scanner.nextInt();

				// operaciones de caja
				switch (opcion) {
				case 1:
					System.out.println("INGRESO DE DINERO ");

					System.out.println(
							"ingrese el tipo de caja donde va a depositar \n caja de ahorro -1 \n cuenta corriente -2");
					opcion = scanner.nextInt();

					switch (opcion) {
					case 1:
						System.out.println("usted eligio CAJA DE AHORRO\n");
						System.out.println("ingrese el monto a depositar");
						transaccion = scanner.nextInt();
						dineroGuardado = (int) clienteDao.listar().get(idCliente).getMontoCbu();
						total = transaccion + dineroGuardado;
						client.setMontoCbu(total);
						clienteDao.registrar(client, 0);

						break;

					case 2:
						System.out.println("usted eligio CUENTA CORRIENTE");
						System.out.println("ingrese el monto a depositar");
						transaccion = scanner.nextInt();
						dineroGuardado = (int) clienteDao.listar().get(idCliente).getMontoCc();
						total = transaccion + dineroGuardado;
						client.setMontoCc(total);
						clienteDao.registrar(client, 1);

						break;
					}

					break;

				case 2:
					System.out.println("RETIRO DE DINERO ");
					System.out.println(
							"ingrese el tipo de caja donde va retirar dinero  \n caja de ahorro -1 \n cuenta corriente -2");
					opcion = scanner.nextInt();
					switch (opcion) {

					case 1:
						System.out.println("CAJA DE AHORRO");
						System.out.println("ingrese el monto a retirar");

						transaccion = scanner.nextInt();// voy a retirar

						dineroGuardado = (int) clienteDao.listar().get(idCliente).getMontoCbu();// lo que en caja

						total = dineroGuardado - transaccion;

						client.setMontoCbu(total);///////

						clienteDao.eliminar(client, 0);
						// registra el retiro de billete
						billete.setRetiro(transaccion);
						billeteDao.eliminar(billete, 0);

						billeteDao.retiroBilletes(transaccion);

						break;

					case 2:
						System.out.println("CUENTA CORRIENTE");
						System.out.println("ingrese el monto a retirar");
						transaccion = scanner.nextInt();// voy a retirar

						dineroGuardado = (int) clienteDao.listar().get(idCliente).getMontoCc();// lo que en caja

						total = dineroGuardado - transaccion;

						client.setMontoCc(total);///////

						clienteDao.eliminar(client, 1);
						// registra el retiro de billete
						billete.setRetiro(transaccion);
						billeteDao.eliminar(billete, 1);

						billeteDao.retiroBilletes(transaccion);
						break;
					}

					break;

				case 3:
					System.out.println("CONSULTA DE CLIENTE");
					System.out.println(clienteDao.listar().get(idCliente));
					break;

				}

			} else

			{
				System.out.println("error de logueo ");
			}

			scanner.close();

		}

	}
}