package com.pablovallejos.pojo;

public class  Persona {
	private int id;
	private String apellido;
	private String nombre;
	private int dni;
	private int tel;
	private String direccion;
	private String userCajero;
	private String passCajero;
	private double montoCbu;
	private double montoCc;
	
	public Persona() {}

	public Persona(int id,String apellido, String nombre, int dni, int tel, String direccion, String userCajero,
			String passCajero, double montoCbu, double montoCc) {
		
		this.apellido = apellido;
		this.nombre = nombre;
		this.dni = dni;
		this.tel = tel;
		this.direccion = direccion;
		this.userCajero = userCajero;
		this.passCajero = passCajero;
		this.montoCbu = montoCbu;
		this.montoCc = montoCc;
	}

	public String getApellido() {
		return apellido;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getUserCajero() {
		return userCajero;
	}

	public void setUserCajero(String userCajero) {
		this.userCajero = userCajero;
	}

	public String getPassCajero() {
		return passCajero;
	}

	public void setPassCajero(String passCajero) {
		this.passCajero = passCajero;
	}

	public double getMontoCbu() {
		return montoCbu;
	}

	public void setMontoCbu(double montoCbu) {
		this.montoCbu = montoCbu;
	}

	public double getMontoCc() {
		return montoCc;
	}

	public void setMontoCc(double montoCc) {
		this.montoCc = montoCc;
	}

	@Override
	public String toString() {
		return "Usuario:\n apellido = " + apellido + ",\n nombre = " + nombre + ",\n dni = " + dni + ",\n tel = " + tel + ",\n direccion = "
				+ direccion + ",\n userCajero = " + userCajero + ",\n passCajero = " + passCajero + ",\n montoCbu = " + montoCbu
				+ ",\n montoCc = " + montoCc + "";
	}
	


}
