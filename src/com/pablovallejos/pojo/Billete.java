package com.pablovallejos.pojo;

public class Billete {
	private int id;
	private int numeracion;//su numeracion
	private int cantBillete;
	private int retiro;
	
	public Billete() {}

	public Billete(int id, int numeracion, int cantBillete, int retiro) {
		super();
		this.id = id;
		this.numeracion = numeracion;
		this.cantBillete = cantBillete;
		this.retiro = retiro;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumeracion() {
		return numeracion;
	}

	public void setNumeracion(int numeracion) {
		this.numeracion = numeracion;
	}

	public int getCantBillete() {
		return cantBillete;
	}

	public void setCantBillete(int cantBillete) {
		this.cantBillete = cantBillete;
	}

	public int getRetiro() {
		
		return retiro;
	}

	public void setRetiro(int retiro) {
		this.retiro = retiro;
	}

	@Override
	public String toString() {
		return "Billete [id=" + id + ", numeracion=" + numeracion + ", cantBillete=" + cantBillete + ", totalDepo="
				+ retiro + "]";
	}

	

	
	
	
}
