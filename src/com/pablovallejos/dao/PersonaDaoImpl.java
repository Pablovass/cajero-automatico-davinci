package com.pablovallejos.dao;

import java.util.ArrayList;
import java.util.List;

import com.pablovallejos.pojo.Persona;

public class PersonaDaoImpl implements PersonaDao {

	@Override
	public List<Persona> listar() {

		List<Persona> listaP = new ArrayList<>();
		Persona p = new Persona();
		p.setId(1);
		p.setApellido("Rodriguez");
		p.setNombre("gaston");
		p.setDni(3256545);
		p.setTel(011433423);
		p.setDireccion("Av.Corriente 3222");
		p.setUserCajero("gastonUser");
		p.setPassCajero("user12345");
		p.setMontoCbu(200.00);
		p.setMontoCc(200.00);

		listaP.add(p);

		p = new Persona();
		p.setId(2);
		p.setApellido("Wasp");
		p.setNombre("Pablo");
		p.setDni(32542);
		p.setTel(0114334432);
		p.setDireccion("av San Martin 3222");
		p.setUserCajero("pabloUser");
		p.setPassCajero("user12345");
		p.setMontoCbu(30000.00);
		p.setMontoCc(30001.00);

		listaP.add(p);
		;
		return listaP;
	}


	@Override
	public int total() {
		int c = 2;
		return c;

	}


	// solo para ingresos de dinero
	@Override
	public void registrar(Persona persona, int idCuenta) {
		switch (idCuenta) {
		case 0:
			System.out.println("registro de ingreso caja de ahorro");
			System.out.println("el monto total es: " + persona.getMontoCbu());
			break;
		case 1:
			System.out.println("registro de pago caja cuenta corriente");
			System.out.println("el monto total es: " + persona.getMontoCc());
			break;

		}
	}

	@Override
	public void eliminar(Persona persona, int idCuenta) {
	

		switch (idCuenta) {
		case 0:
			 System.out.println("retiro de pago caja de ahorro");
			 System.out.println("el monto total es: " + persona.getMontoCbu());
			break;
		case 1:
			System.out.println("retiro de pago caja cuenta corriente");
			System.out.println("el monto total es: " + persona.getMontoCc());
			break;

		}

	}

	

}