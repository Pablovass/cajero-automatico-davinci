package com.pablovallejos.dao;

import java.util.ArrayList;
import java.util.List;

import com.pablovallejos.pojo.Billete;


public class BilleteDaoImpl implements BilleteDao {

	private int contador;

	@Override
	public List<Billete> listar() {

		List<Billete> listaB = new ArrayList<>();
		Billete b = new Billete();

		b.setId(1);
		b.setNumeracion(2);
		b.setCantBillete(32);
		listaB.add(b);

		b = new Billete();
		b.setId(2);
		b.setNumeracion(5);
		b.setCantBillete(50);
		listaB.add(b);

		b = new Billete();
		b.setId(3);
		b.setNumeracion(10);
		b.setCantBillete(40);
		listaB.add(b);

		b = new Billete();
		b.setId(4);
		b.setNumeracion(20);
		b.setCantBillete(60);
		listaB.add(b);

		b = new Billete();
		b.setId(5);
		b.setNumeracion(50);
		b.setCantBillete(100);
		listaB.add(b);

		b = new Billete();
		b.setId(6);
		b.setNumeracion(100);
		b.setCantBillete(1000);
		listaB.add(b);

		contador = b.getId();
		return listaB;

	}

	@Override
	public void registrar(Billete t, int t1) {
		// TODO Auto-generated method stub

	}

	// retiro de billetes
	@Override
	public void eliminar(Billete billete, int idCaja) {
		
		
		switch (idCaja) {
		case 0:
			System.out.println("CAJA DE AHORRO");
			System.out.println("Usted retiro "+billete.getRetiro()+" pesos");	
			
			break;
		case 1:
			System.out.println("CUENTA CORRIENTE");
			System.out.println("Usted retiro "+billete.getRetiro()+" pesos");	
			
			
			break;

		}
		// TODO Auto-generated method stub

	}

	// total de billete de la caja
	// parecido a un set billete
	@Override
	public int total() {
		int c = contador;
		return c;

	}

	

	@Override
	public void retiroBilletes(int retiroDinero) {
		int b;
		
		Billete billete = new Billete();
		billete.setRetiro(retiroDinero);
		
		billete.setNumeracion(100);
		b=calculaBillete(billete);
		System.out.println(b+" Billetes de "+billete.getNumeracion());
		
		billete.setNumeracion(50);
		b=calculaBillete(billete);
		System.out.println(b+" Billetes de "+billete.getNumeracion());
		
		billete.setNumeracion(20);
		b=calculaBillete(billete);
		System.out.println(b+" Billetes de "+billete.getNumeracion());
		
		billete.setNumeracion(10);
		b=calculaBillete(billete);
		System.out.println(b+" Billetes de "+billete.getNumeracion());
		
		
		billete.setNumeracion(5);
		b=calculaBillete(billete);
		System.out.println(b+" Billetes de "+billete.getNumeracion());
		
		
	}

	@Override
	public int calculaBillete(Billete b) {
			
			int bi = b.getRetiro() / b.getNumeracion();
			int aux= b.getRetiro() % b.getNumeracion();
			b.setRetiro(aux);
		
			return bi;
		}
	
}
