package com.pablovallejos.dao;

import java.util.List;

public interface CRUD<T> {
	
	// GET
	List <T>listar();


	//POST -PUT
	void registrar(T t,int t1);

	//DELETE
	void eliminar(T t,int t1);

	//update
	int total();
	 
}
