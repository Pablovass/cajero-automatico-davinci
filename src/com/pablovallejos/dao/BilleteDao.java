package com.pablovallejos.dao;



import com.pablovallejos.pojo.Billete;


public interface BilleteDao extends CRUD<Billete>  {

	public int calculaBillete(Billete b);
	public void retiroBilletes(int retiroDinero);

}
